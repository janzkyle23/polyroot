#include "Polynomial.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

using namespace std;

void Polynomial::printPolynomial()
{
  cout << "Polynomial:" << endl;
  for(int i = 0; i <= n; i++)
  {
    cout << fixed << setprecision(5) << terms.at(i) << " x^" << n-i << endl;
  }
  cout << endl;
}

void Polynomial::GetB()
{
  for (int i = 0; i <= n; i++)
  {
    if(i == 0)
    {
      B.push_back(A.at(0));
    }
    else if(i == 1)
    {
      B.push_back(A.at(1) + r*B.at(0));
    }
    else
    {
      B.push_back(A.at(i) + r*B.at(i-1) + s*B.at(i-2));
    }
  }
  b0 = B.at(n);
  b1 = B.at(n-1);
}

void Polynomial::GetC()
{
  for (int i = 0; i < n; i++)
  {
    if(i == 0)
    {
      C.push_back(B.at(0));
    }
    else if(i == 1)
    {
      C.push_back(B.at(1) + r*C.at(0));
    }
    else
    {
      C.push_back(B.at(i) + r*C.at(i-1) + s*C.at(i-2));
    }
  }
  c1 = C.at(n-1);
  c2 = C.at(n-2);
  c3 = C.at(n-3);
}

void Polynomial::System()
{
  dr = -(c3*(((c1*b1)/c2 - b0) / (((-c1*c3)/c2) + c2))+b1) / c2;
  ds = ((c1*b1)/c2 - b0) / (((-c1*c3)/c2) + c2);
}

void Polynomial::QuadRoots(double a, double b, double c)
{
  complex<double> discriminant( pow(b, 2)-4*a*c);
  if (discriminant.real() >= 0.0)
  {
    roots.push_back((-b + sqrt(discriminant.real())) / 2);
    roots.push_back((-b - sqrt(discriminant.real())) / 2);
  }
  else
  {
    roots.push_back((-b/2 + (sqrt(-discriminant.real())/2)*1i));
    roots.push_back((-b/2 - (sqrt(-discriminant.real())/2)*1i));
  }
};

void Polynomial::printRoots()
{
  cout << "Roots:" << endl;
  for (int i = 0; i < N; i++)
  {
    cout << fixed << setprecision(5) << roots.at(i) << endl;
  }
  cout << endl;
};

void Polynomial::evaluateRoots()
{
  complex<double> evaluate;
  cout << "Evaluating polynomial at identified roots:" << endl;

  for (int i = 0; i < N; i++)
  {
    evaluate = 0;
    for (double j = 0; j <= N; j++)
    {
      evaluate += terms.at(j) * pow(roots.at(i),N-j);
    }
    cout << fixed << setprecision(5) << "f" << roots.at(i) << " = " << evaluate.real() << endl;
  }
};



