#include "Polynomial.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>
#include <algorithm>

using namespace std;

ifstream inFile;

bool GetInputFile(const char* txtfile)
{
  cout << "Finding file: " << txtfile << endl;

  inFile.open(txtfile);

  if(!inFile)
  {
    return false;
  }

  cout << txtfile << " found!" << endl << endl;
  return true;
}

int main(int argc, char *argv[])
{
  const char* txtfile;

  bool success = false;

  if(argc == 1)
  {
    cout << "filename: ";
    string filename;
    cin >> filename;
    txtfile = filename.c_str();
  }
  else
  {
    txtfile = argv[1];
  }

  success = GetInputFile(txtfile);

  if(!success)
  {
    while(!success)
    {
      cout << endl << "File not found." << endl;
      cout << "filename: ";
      string filename;
      cin >> filename;
      txtfile = filename.c_str();
      success = GetInputFile(txtfile);
    }
  }

  Polynomial poly;
  inFile >> poly.N;
  poly.n = poly.N;

  for(int i = 0; i <= poly.N; i++)
  {
    double term;
    inFile >> term;
    poly.terms.push_back(term);
  }
  reverse(poly.terms.begin(),poly.terms.end());
  poly.A = poly.terms;

  poly.printPolynomial();

  while (poly.n > 2)
  {
    poly.r = poly.A.at(1)/poly.A.at(0);
    poly.s = poly.A.at(2)/poly.A.at(0);
    poly.dr = 0;
    poly.ds = 0;

    do{
      poly.B.clear();
      poly.C.clear();
      poly.GetB();
      poly.GetC();
      poly.System();
      poly.r += poly.dr;
      poly.s += poly.ds;
    }while(abs(poly.dr/poly.r)*1000 > 0.001 || abs(poly.ds/poly.s)*1000 > 0.001);

    poly.QuadRoots(1, -1*poly.r, -1*poly.s);
    poly.A.clear();
    poly.A = poly.B;
    poly.A.pop_back();
    poly.A.pop_back();
    poly.n-=2;
  }

  if (poly.n == 2)
  {
    poly.QuadRoots(poly.A.at(0), poly.A.at(1), poly.A.at(2));
  }
  else
  {
    poly.roots.push_back(-poly.A.at(1)/poly.A.at(0));
  }

  poly.printRoots();
  poly.evaluateRoots();

  return 0;
}
