#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H
#include <vector>
#include <complex>

struct Polynomial {

  std::vector<double> terms, A, B, C;
  double r, s, dr, ds, b0, b1, c1, c2, c3;
  int N, n;
  std::vector< std::complex<double> >roots;

  void printPolynomial();
  void GetB();
  void GetC();
  void System();
  void QuadRoots(double a, double b, double c);
  void printRoots();
  void evaluateRoots();
};

#endif // POLYNOMIAL_H
